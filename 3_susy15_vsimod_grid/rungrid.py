import os

samples = {
    # 'JZ0W' : 
    # 'JZ1W' : 
    'JZ2W' : 'user.rnewhous.364702.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2WithSW.mc16e.DAOD_SUSY15_v4_EXT0',
    'JZ3W' : 'user.rnewhous.364703.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3WithSW.mc16e.DAOD_SUSY15_v4_EXT0',
    'JZ4W' : 'user.rnewhous.364704.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4WithSW.mc16e.DAOD_SUSY15_v4_EXT0',

#    'JZ5W' : 'user.rnewhous.364705.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5WithSW.mc16e.DAOD_SUSY15_v3_EXT0',
#    'JZ6W' : 'user.rnewhous.364706.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6WithSW.mc16e.DAOD_SUSY15_v3_EXT0',
#    'JZ7W' : 'user.rnewhous.364707.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7WithSW.mc16e.DAOD_SUSY15_v3_EXT0',

#    'JZ8W' : 'user.rnewhous.364708.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ8WithSW.mc16e.DAOD_SUSY15_v4_EXT0',
#    'JZ9W' : 'user.rnewhous.364709.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9WithSW.mc16e.DAOD_SUSY15_v4_EXT0',
#    'JZ10W' : 'user.rnewhous.364710.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ10WithSW.mc16e.DAOD_SUSY15_v4_EXT0',
#    'JZ11W' : 'user.rnewhous.364711.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ11WithSW.mc16e.DAOD_SUSY15_v4_EXT0',
#    'JZ12W' : 'user.rnewhous.364712.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12WithSW.mc16e.DAOD_SUSY15_v4_EXT0',
}

for key, sample in samples.items():
    command = """
xAH_run.py --force \
--config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm_rerunVSIconfigs.py \
--files {sample} \
--inputRucio \
--extraOptions="--rerunVSI_LRTR3test  --isSUSY15" \
--isMC \
prun \
--optGridOutputSampleName {outds}_v3 \
--optGridExcludedSite NET2 \
""".format(sample=sample, outds=sample.replace('_EXT0', '.ntuple'))
    print("Submitting")
    print(command)
    os.system(command)
    print("Finished")
    # break

# --optGridNFiles 1

# --optGridExcludedSite BNL \
# --optGridExcludedSite NET2 \
