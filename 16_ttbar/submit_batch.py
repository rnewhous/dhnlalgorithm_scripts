import os
import time


# make input files using: for i in $(ls -1d /data/hnl/ntuples/v5p0_ntuples/mc/*); do ls $i/* -1d; done > input_files.txt
# os.system("for i in $(ls -1d /data/hnl/ntuples/v4p1_ntuples/mc/*); do ls $i/* -1d; done > input_files.txt")
with open('files.txt') as f:
    for infile in f.readlines():
        infile = infile.strip()
        # get mc generator info
        file_id = infile.split('/')[-1][29:35]
        print(file_id)
        # get reconstruction info
        work_dir = '/data/hnl/ntuples/ttbar_v5p0_ntuples/'
        submit_dir = work_dir + file_id
        # skip if already done

        # set command with the correct input variables
        command  = f'qsub -v INPUT_FILE={infile},SUBMIT_DIR={submit_dir},WORK_DIR={work_dir} -N dhnl_algorithm_{file_id} -e /home/newhouse/tmp/grid_logs/dhnl_algorithm_{file_id}.err -o /home/newhouse/tmp/grid_logs/dhnl_algorithm_{file_id}.log /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/run/16_ttbar/run_grid.pbs'
        # run
        print(infile, file_id)

        exit_code = os.system(command)
        # success?
        print(exit_code, 'running', file_id)

        time.sleep(0.1)

        # max_job_count=300
        # def run_count(): return int(os.popen('qstat -r|  wc -l').read().strip())
        # def idle_count(): return int(os.popen('qstat -i|  wc -l').read().strip())
        # while run_count() + idle_count() > max_job_count:
        #     time.sleep(10)
