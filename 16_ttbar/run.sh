# local
xAH_run.py \
--config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/source/DHNLAlgorithm/data/config_DHNLAlgorithm.py \
--inputList \
--files files.txt \
--force \
--submitDir ttbarSubmitDir \
--isMC \
direct



# grid
# xAH_run.py --force --config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/build/generic/data/DHNLAlgorithm/config_DHNLAlgorithm.py --inputRucio --files mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.DAOD_RPVLL.e6337_e5984_s3126_r11915_r11748 --isMC --submitDir submitDir_15 prun --optGridOutputSampleName group.phys-exotics.mc16_13TeV.410470.r11915.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.dhnl_ntuple_v5p0.v1 --optSubmitFlags="--official --voms atlas:/atlas/phys-exotics/Role=production"
# xAH_run.py --force --config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/build/generic/data/DHNLAlgorithm/config_DHNLAlgorithm.py --inputRucio --files mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.DAOD_RPVLL.e6337_e5984_s3126_r11916_r11748 --isMC --submitDir submitDir_16 prun --optGridOutputSampleName group.phys-exotics.mc16_13TeV.410470.r11916.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.dhnl_ntuple_v5p0.v1 --optSubmitFlags="--official --voms atlas:/atlas/phys-exotics/Role=production"
