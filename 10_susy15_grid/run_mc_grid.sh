# run mc samples on grid
WorkDir_DIR=/home/newhouse/public/Analysis/HNL/DHNLAlgorithm/build/generic
NTUP_VERSION=v5p1
echo > tmp_submit_script.sh

for infile in \
official_lists/DAOD_EXOT29_mc16a_promptelectron.txt \
official_lists/DAOD_EXOT29_mc16a_promptmuon.txt \
official_lists/DAOD_EXOT29_mc16d_promptelectron.txt \
official_lists/DAOD_EXOT29_mc16d_promptmuon.txt \
official_lists/DAOD_EXOT29_mc16e_promptelectron.txt \
official_lists/DAOD_EXOT29_mc16e_promptmuon.txt \
; do
    while read line; do
        if [[ $line == \#* ]]; then # skip commented lines
            continue 
        fi

        # just print the name of the output file
        ntuple=group.phys-exotics.$(echo $line | cut -d'.' -f 1,2,5,6).dhnl_ntuple_$NTUP_VERSION.v?_tree.root
        # check if file exists
        FILE=/data/hnl/ntuples/${NTUP_VERSION}_ntuples/mc/$ntuple/;
        FILE=/data/hnl/ntuples/tracking_syst_ntuples/mc/$ntuple/;
        # echo $FILE
        if [ ! -f $FILE* ]; then
            echo "$ntuple not downloaded."
            echo "# $ntuple" >> tmp_submit_script.sh

            echo xAH_run.py --force \
            --config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
            --inputRucio --files $line \
            --isMC \
            --extraOptions=\"--isDerivation\" \
            prun --optGridOutputSampleName group.phys-exotics.$(echo $line | cut -d'.' -f 1,2,5,6).dhnl_ntuple_$NTUP_VERSION.v1 \
            --optSubmitFlags=\"--official --voms atlas:/atlas/phys-exotics/Role=production\" \
            >> tmp_submit_script.sh
            echo >> tmp_submit_script.sh
        fi
    done < $infile
done

