# run mc samples on grid
WorkDir_DIR=/home/newhouse/public/Analysis/HNL/DHNLAlgorithm/build/generic
NTUP_VERSION=v9p0
RUN_ATTEMPT=1

for sublist in \
mc16a_oddsandends \
mc16d_oddsandends \
mc16e_oddsandends \
mc16a_promptelectron \
mc16a_promptmuon \
mc16e_promptelectron \
mc16e_promptmuon \
mc16d_promptelectron \
mc16d_promptmuon \
; do
    echo > tmp_submit_script_$sublist.sh
    infile=../official_lists/DAOD_EXOT29_$sublist.txt
    while read line; do
        if [[ $line == \#* ]]; then # skip commented lines
            continue 
        fi

        # just print the name of the output file
        ntuple=group.phys-exotics.$(echo $line | cut -d'.' -f 1,2,5,6).dhnl_ntuple_$NTUP_VERSION.v?_tree.root
        # check if file exists
        FILE=/data/hnl/ntuples/${NTUP_VERSION}_ntuples/mc/$ntuple/;
        # FILE=/data/hnl/ntuples/tracking_syst_ntuples/mc/$ntuple/;
        # echo $FILE
        if [ ! -f $FILE* ]; then
            echo "$ntuple not downloaded."
            echo "# $ntuple" >> tmp_submit_script_$sublist.sh

            echo xAH_run.py --force \
            --config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
            --inputRucio --files $line \
            --isMC \
            --submitDir submitDir_$sublist \
            --extraOptions=\"--isDerivation --runAllSyst --samplePeriod ${sublist:0:5}\" \
            prun --optGridOutputSampleName group.phys-exotics.$(echo $line | cut -d'.' -f 1,2,5,6).dhnl_ntuple_$NTUP_VERSION.v$RUN_ATTEMPT \
            --optSubmitFlags=\"--official --voms atlas:/atlas/phys-exotics/Role=production\" \
            >> tmp_submit_script_$sublist.sh
            echo >> tmp_submit_script_$sublist.sh
        fi
    done < $infile
done

# ./tmp_submit_script_mc16a_promptelectron.sh & ./tmp_submit_script_mc16a_promptmuon.sh & ./tmp_submit_script_mc16d_promptelectron.sh & ./tmp_submit_script_mc16d_promptmuon.sh & ./tmp_submit_script_mc16e_promptelectron.sh & ./tmp_submit_script_mc16e_promptmuon.sh &