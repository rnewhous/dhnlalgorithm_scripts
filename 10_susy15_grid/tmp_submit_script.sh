
# group.phys-exotics.mc16_13TeV.311629.DAOD_EXOT29.e7422_e5984_a875_r11891_r11748_p4482.dhnl_ntuple_v5p1.v?_tree.root
xAH_run.py --force --config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/build/generic/data/DHNLAlgorithm/config_DHNLAlgorithm.py --inputRucio --files mc16_13TeV.311629.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_7p5G_lt1dd_el.deriv.DAOD_EXOT29.e7422_e5984_a875_r11891_r11748_p4482 --isMC --extraOptions="--isDerivation" prun --optGridOutputSampleName group.phys-exotics.mc16_13TeV.311629.DAOD_EXOT29.e7422_e5984_a875_r11891_r11748_p4482.dhnl_ntuple_v5p1.v1 --optSubmitFlags="--official --voms atlas:/atlas/phys-exotics/Role=production"

