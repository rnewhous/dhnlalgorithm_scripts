import os
import time
import subprocess

with open('submit_data_job.pbs', 'w') as f:
    print(
        """
#PBS -N dhnlalg_grid_submit
#PBS -l walltime=48:00:00
#PBS -l nodes=1:ppn=1
#PBS -e /home/newhouse/tmp/grid_logs/dhnlalg_grid_submit.err
#PBS -o /home/newhouse/tmp/grid_logs/dhnlalg_grid_submit.log

# Check variables
echo INPUT_CONTAINER $INPUT_CONTAINER
echo OUTPUT_CONTAINER $OUTPUT_CONTAINER
echo SUBDIR $SUBDIR

# make submission directory
mkdir -p /home/newhouse/tmp/grid_submissions/$SUBDIR
cd /home/newhouse/tmp/grid_submissions/$SUBDIR

# setup environment 
source /home/newhouse/setup/setupATLAS.sh
export RUCIO_ACCOUNT=phys-exotics
lsetup rucio
echo d9gkzPui2k8Ii2020grid | voms-proxy-init -voms atlas:/atlas/phys-exotics/Role=production -hours 240
lsetup panda
asetup AnalysisBase,21.2.121
source /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/build/generic/setup.sh

# run
xAH_run.py --force --config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm.py --inputRucio --files $INPUT_CONTAINER --extraOptions="--isSUSY15" prun --optGridOutputSampleName $OUTPUT_CONTAINER --optSubmitFlags="--official --voms atlas:/atlas/phys-exotics/Role=production"

        """,
        file=f
    )

load_grid_proxy = False
if load_grid_proxy: 
    for i in range(1,23): 
        if i in [7, 20, 22]: continue
        print (f'loading proxy on u{i}')
        os.system(f"ssh u{i} 'setupATLAS; lsetup rucio; echo d9gkzPui2k8Ii2020grid | voms-proxy-init -voms atlas:/atlas/phys-exotics/Role=production -hours 240' &")
        print (i)
    exit()

with open('create_dataset_scripts/containers_data18.txt') as f:
    for input_container in f.readlines():
        # get variables for this job
        input_container = input_container.strip()
        subdir = '/'.join(input_container.split('.')[0:2])
        data_year = input_container.split('_13TeV.')[0]
        dsid = input_container.split('.')[1]
        output_container = f'group.phys-exotics.{data_year}_dhnl_ntuple_v4p0.{dsid}.v1'
        print(output_container)
        # set command with the correct input variables
        command  = f'qsub -v INPUT_CONTAINER={input_container},SUBDIR={subdir},OUTPUT_CONTAINER={output_container} -N submit_data_{dsid} -e /home/newhouse/tmp/grid_logs/submit_data_{dsid}.err -o /home/newhouse/tmp/grid_logs/submit_data_{dsid}.log /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/run/10_susy15_grid/submit_data_job.pbs'
        # print(command)
        # break
        
        # run
        exit_code = os.system(command)
        # success?
        print(exit_code, 'running', dsid)

        time.sleep(0.01)

        # max_job_count=300
        # def run_count(): return int(os.popen('qstat -r|  wc -l').read().strip())
        # def idle_count(): return int(os.popen('qstat -i|  wc -l').read().strip())
        # while run_count() + idle_count() > max_job_count:
        #     time.sleep(10)