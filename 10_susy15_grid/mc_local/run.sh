# 'ls' -1d /data/newhouse/HNL/DHNL/mc16_13TeV.312993.Pythia8EvtGen_A14NNPDF23LO_WelHNL50_12p5G_lt10dd_ee.deriv.DAOD_EXOT29.e7902_e5984_a875_r11915_r11748_p4482/* > files.txt

# xAH_run.py --force \
# --config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/build/generic/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
# --inputList --files files.txt \
# --submitDir submitDir \
# --isMC \
# --extraOptions="--isDerivation --samplePeriod mc16a" \
# --nevents 100 \
# direct

############

# 'ls' -1d /data/newhouse/HNL/EXOT29/mc16_13TeV.312962.Pythia8EvtGen_A14NNPDF23LO_WelHNL50_4G_lt1dd_ee.deriv.DAOD_EXOT29.e7902_e5984_a875_r11916_r11748_p4482/* > files_312962.txt

# xAH_run.py --force \
# --config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/build/generic/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
# --inputList --files files_312962.txt \
# --submitDir submitDir_312962 \
# --isMC \
# --extraOptions="--isDerivation --samplePeriod mc16d" \
# direct &


# 'ls' -1d /data/newhouse/HNL/EXOT29/mc16_13TeV.311624.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_5G_lt10dd_el.deriv.DAOD_EXOT29.e7422_e5984_a875_r11916_r11748_p4482/* > files_311624.txt

# xAH_run.py --force \
# --config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/build/generic/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
# --inputList --files files_311624.txt \
# --submitDir submitDir_311624 \
# --isMC \
# --extraOptions="--isDerivation --samplePeriod mc16d" \
# direct &


# 'ls' -1d /data/newhouse/HNL/EXOT29/mc16_13TeV.311633.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_10G_lt10dd.deriv.DAOD_EXOT29.e7422_e5984_a875_r11916_r11748_p4482/* > files_311633.txt

# xAH_run.py --force \
# --config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/build/generic/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
# --inputList --files files_311633.txt \
# --submitDir submitDir_311633 \
# --isMC \
# --extraOptions="--isDerivation --samplePeriod mc16d" \
# direct &


# 'ls' -1d /data/newhouse/HNL/EXOT29/mc16_13TeV.311628.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_7p5G_lt100dd.deriv.DAOD_EXOT29.e7422_e5984_a875_r11916_r11748_p4482/* > files_311628.txt

# xAH_run.py --force \
# --config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/build/generic/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
# --inputList --files files_311628.txt \
# --submitDir submitDir_311628 \
# --isMC \
# --extraOptions="--isDerivation --samplePeriod mc16d" \
# direct &

####


TAG=313487_a
'ls' -1d /data/newhouse/HNL/EXOT29/mc16_13TeV.313487.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_10G_lt100dd_tt.deriv.DAOD_EXOT29.e8368_a875_r11915_p4482/* > files_$TAG.txt

xAH_run.py --force \
--config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/build/generic/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
--inputList --files files_$TAG.txt \
--submitDir submitDir_$TAG \
--isMC \
--extraOptions="--isDerivation --samplePeriod mc16a" \
direct &


TAG=313487_d
'ls' -1d /data/newhouse/HNL/EXOT29/mc16_13TeV.313487.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_10G_lt100dd_tt.deriv.DAOD_EXOT29.e8368_a875_r11916_p4482/* > files_$TAG.txt

xAH_run.py --force \
--config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/build/generic/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
--inputList --files files_$TAG.txt \
--submitDir submitDir_$TAG \
--isMC \
--extraOptions="--isDerivation --samplePeriod mc16d" \
direct &


TAG=313487_e
'ls' -1d /data/newhouse/HNL/EXOT29/mc16_13TeV.313487.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_10G_lt100dd_tt.deriv.DAOD_EXOT29.e8368_a875_r11891_p4482/* > files_$TAG.txt

xAH_run.py --force \
--config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/build/generic/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
--inputList --files files_$TAG.txt \
--submitDir submitDir_$TAG \
--isMC \
--extraOptions="--isDerivation --samplePeriod mc16e" \
direct &


