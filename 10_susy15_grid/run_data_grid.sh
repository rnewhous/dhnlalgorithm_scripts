# # # run data samples on grid

# # # data15
# # xAH_run.py --force \
# # --config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
# # --inputRucio --files user.rnewhous:data15_13TeV.physics_Main.deriv.DAOD_SUSY15.r11969_r11784_p4072_p4362 \
# # --extraOptions="--isSUSY15" \
# # prun --optGridOutputSampleName user.$CERN_USER.data15_ntuple_v4p0.v1 \

# # # # data16
# # xAH_run.py --force \
# # --config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
# # --inputRucio --files user.rnewhous:data16_13TeV.physics_Main.deriv.DAOD_SUSY15.r11969_r11784_p4072_p4362 \
# # --extraOptions="--isSUSY15" \
# # prun --optGridOutputSampleName user.$CERN_USER.data16_ntuple_v4p0.v1 \

# # # # data17
# # xAH_run.py --force \
# # --config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
# # --inputRucio --files user.rnewhous:data17_13TeV.physics_Main.deriv.DAOD_SUSY15.r11969_r11784_p4072_p4362 \
# # --extraOptions="--isSUSY15" \
# # prun --optGridOutputSampleName user.$CERN_USER.data17_ntuple_v4p0.v1 \

# # data18
# xAH_run.py --force \
# --config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
# --inputRucio --files user.rnewhous:data18_13TeV.physics_Main.deriv.DAOD_SUSY15.r11969_r11784_p4072_p4362 \
# --extraOptions="--isSUSY15" \
# prun --optGridOutputSampleName user.$CERN_USER.data18_ntuple_v4p0.v2 \


# # # voms-proxy-init -voms atlas:/atlas/phys-exotics/Role=production -hours 240
# # # --optSubmitFlags="--official" \
# # # data18 container 
# # xAH_run.py --force \
# # --config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
# # --inputRucio --files user.rnewhous:data18_13TeV.physics_Main.deriv.DAOD_SUSY15.r11969_r11784_p4072_p4362.container \
# # --extraOptions="--isSUSY15" \
# # prun \
# # --optGridOutputSampleName group.phys-exotics.data18_dhnl_ntuple_v4p0.v1 \
# # --optSubmitFlags="--official" \

# # data18 for nicky
# xAH_run.py --force \
# --config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
# --inputRucio --files group.phys-exotics.data18_13TeV.periodAllYear.physics_Main.PhysCont.SUSY15.DVskim.prod25_v04_v2_EXT0 \
# --extraOptions="--isSUSY15" \
# prun --optGridOutputSampleName group.phys-exotics.data18_ntuple_v4p2.v1 \
# --optSubmitFlags="--official --voms atlas:/atlas/phys-exotics/Role=production"\

# # data17 local test
# xAH_run.py --force \
# --config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
# --files /data/newhouse/HNL/DHNL/data17_13TeV.00339396.physics_Main.deriv.DAOD_EXOT29.r11969_r11784_p4072_p4491/DAOD_EXOT29.24963031._000050.pool.root.1 \
# --extraOptions="--isDerivation --samplePeriod data17" \
# --nevents 1000 \
# direct 

# data 15-17 exot29
# xAH_run.py --force \
# --config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
# --inputRucio --files group.phys-exotics.data15_13TeV.physics_Main.deriv.DAOD_EXOT29.r11969_r11784_p4072_p4491 \
# --extraOptions="--isDerivation --samplePeriod data15" \
# prun --optGridOutputSampleName group.phys-exotics.data15_ntuple_v8p0.v1 \
# --optGridNFilesPerJob=5.0 \
# --optSubmitFlags="--official --voms atlas:/atlas/phys-exotics/Role=production"\

# xAH_run.py --force \
# --config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
# --inputRucio --files group.phys-exotics.data16_13TeV.physics_Main.deriv.DAOD_EXOT29.r11969_r11784_p4072_p4491 \
# --extraOptions="--isDerivation --samplePeriod data16" \
# prun --optGridOutputSampleName group.phys-exotics.data16_ntuple_v8p0.v2 \
# --optGridNFilesPerJob=5.0 \
# --optSubmitFlags="--official --voms atlas:/atlas/phys-exotics/Role=production"\

xAH_run.py --force \
--config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
--inputRucio --files group.phys-exotics.data17_13TeV.physics_Main.deriv.DAOD_EXOT29.r11969_r11784_p4072_p4491 \
--extraOptions="--isDerivation --samplePeriod data17" \
prun --optGridOutputSampleName group.phys-exotics.data17_ntuple_v8p1.v1 \
--optGridNFilesPerJob=2.0 \
--optSubmitFlags="--official --voms atlas:/atlas/phys-exotics/Role=production"\

# xAH_run.py --force \
# --config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
# --inputRucio --files group.phys-exotics.data18_13TeV.physics_Main.deriv.DAOD_EXOT29.r11969_r11784_p4072_p4491 \
# --extraOptions="--isDerivation --samplePeriod data18" \
# prun --optGridOutputSampleName group.phys-exotics.data18_ntuple_v8p0.v2 \
# --optGridNFilesPerJob=5.0 \
# --optSubmitFlags="--official --voms atlas:/atlas/phys-exotics/Role=production"\
