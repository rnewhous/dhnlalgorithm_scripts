
xAH_run.py \
--config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/source/DHNLAlgorithm/data/config_DHNLAlgorithm.py \
--inputList \
--files files_test_local.txt \
--submitDir submitDir_test_local \
--force \
--isMC \
--nevents 100 \
--extraOptions="--isSUSY15" \
direct
