echo /data/hnl/EXOT_29/data15_13TeV.00000000.debugrec_hlt.deriv.DAOD_EXOT29.g17_49_r12446_r12447_p4431_p4491_tid26205275_00/DAOD_EXOT29.26205275._000003.pool.root.1 > files15.txt
xAH_run.py --force \
--config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
--inputList --files files15.txt \
--extraOptions="--isDerivation --samplePeriod data15" \
--submitDir submit_dir_data15 \
direct &

echo /data/hnl/EXOT_29/data16_13TeV.00000000.debugrec_hlt.deriv.DAOD_EXOT29.g53_r12446_r12447_p4431_p4491_tid26205315_00/DAOD_EXOT29.26205315._000001.pool.root.1 > files16.txt
xAH_run.py --force \
--config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
--inputList --files files16.txt \
--extraOptions="--isDerivation --samplePeriod data16" \
--submitDir submit_dir_data16 \
direct &

echo /data/hnl/EXOT_29/data17_13TeV.00000000.debugrec_hlt.deriv.DAOD_EXOT29.g54_r12446_r12447_p4431_p4491_tid26205343_00/DAOD_EXOT29.26205343._000002.pool.root.1 > files17.txt
echo /data/hnl/EXOT_29/data17_13TeV.00000000.debugrec_hlt.deriv.DAOD_EXOT29.g54_r12446_r12447_p4431_p4491_tid26205343_00/DAOD_EXOT29.26205343._000003.pool.root.1 >> files17.txt
echo /data/hnl/EXOT_29/data17_13TeV.00000000.debugrec_hlt.deriv.DAOD_EXOT29.g54_r12446_r12447_p4431_p4491_tid26205343_00/DAOD_EXOT29.26205343._000004.pool.root.1 >> files17.txt
xAH_run.py --force \
--config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
--inputList --files files17.txt \
--extraOptions="--isDerivation --samplePeriod data17" \
--submitDir submit_dir_data17 \
direct &

echo /data/hnl/EXOT_29/data18_13TeV.00000000.debugrec_hlt.deriv.DAOD_EXOT29.g54_r12446_r12447_p4431_p4491_tid26205377_00/DAOD_EXOT29.26205377._000001.pool.root.1 > files18.txt
xAH_run.py --force \
--config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
--inputList --files files18.txt \
--extraOptions="--isDerivation --samplePeriod data18" \
--submitDir submit_dir_data18 \
direct &
