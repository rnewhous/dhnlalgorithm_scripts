import os
import glob
import time

jzslice = 'JZ9'
SUSY15_path = '/data/hnl/KShort/SUSY15/dijet/user.rnewhous.364709.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9WithSW.mc16e.DAOD_SUSY15_v4_EXT0/'
SUSY15_files = [os.path.join(SUSY15_path, f) for f in os.listdir(SUSY15_path) if os.path.isfile(os.path.join(SUSY15_path, f))]
if not jzslice in SUSY15_path:
    exit(1)

########## Create subdirectories with chunked files

# Yield successive n-sized 
# chunks from l. 
def divide_chunks(l, n): 
      
    # looping till length l 
    for i in range(0, len(l), n):  
        yield l[i:i + n] 
  
# How many elements each 
# list should have 
n = 1
  
chunks = list(divide_chunks(SUSY15_files, n)) 
print(len(chunks))
for i, chunk in enumerate(chunks):
    subdir = f'/data/hnl/KShort/ntuples/dijet/{jzslice}/{i}/'
    os.makedirs(subdir, exist_ok=True)
    with open(subdir+'files.txt', 'w') as f:  # Use file to refer to the file object
        print('\n'.join(chunk), file=f)

    command = f"""
#PBS -N dijet_ntuple_{i}
#PBS -l walltime=48:00:00
#PBS -l nodes=1:ppn=1
#PBS -e /home/newhouse/tmp/grid_logs/dijet_ntuple_{i}.err
#PBS -o /home/newhouse/tmp/grid_logs/dijet_ntuple_{i}.log

source /home/newhouse/setup/pbs_setup.sh
lsetup git
mkdir -p /tmp/newhouse/{subdir}
cd /tmp/newhouse/{subdir}
asetup AnalysisBase,21.2.90
source /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/build/generic/setup.sh

xAH_run.py --force \\
--config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm_rerunVSIconfigs.py \\
--files {subdir}files.txt \\
--inputList \\
--extraOptions="--rerunVSI_LRTR3test  --isSUSY15" \\
--isMC \\
direct 

rm -rf {subdir}submitDir
mv submitDir {subdir}
"""

    with open(subdir+'submit.pbs', 'w') as f:  
        print(command, file=f)

    print(f"Submitting {subdir}submit.pbs")
    # print(command)
    os.system(f'qsub {subdir}submit.pbs')
    print("Finished")
    time.sleep(0.5)

    max_job_count=250
    def run_count(): return int(os.popen('qstat -r|  wc -l').read().strip())
    def idle_count(): return int(os.popen('qstat -i|  wc -l').read().strip())
    while run_count() + idle_count() > max_job_count:
        time.sleep(10)
