# # mc
# # make files.txt
# # 'ls' -1d /data/hnl/KShort/SUSY15/dijet/user.rnewhous.364712.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12WithSW.mc16e.DAOD_SUSY15_v3_EXT0/*.root > files.txt
# # echo /data/hnl/VSI_rerun/mc16_13TeV.311634.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_10G_lt100dd_el_SUSY15_comboLRTd0Test_VSILepMod/DAOD_SUSY15_1.output.pool.root> files.txt
# echo /data/newhouse/HNL/DHNL/mc16_13TeV.311633.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_10G_lt10dd.deriv.DAOD_SUSY15.e7422_e5984_a875_r11916_r11748_p4357/DAOD_SUSY15.23638793._000001.pool.root.1 > files.txt

# # make ntuples
# xAH_run.py \
# --config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/source/DHNLAlgorithm/data/config_DHNLAlgorithm.py \
# --inputList \
# --files files.txt \
# --submitDir submitDir \
# --force \
# --isMC \
# --nevents 1000 \
# --extraOptions="--isSUSY15" \
# direct


# data
# make files.txt
echo /data/hnl/VSI_rerun/data18_13TeV.00364292.physics_Main.deriv.DAOD_SUSY15.r11969_r11784_p4072_p4362/DAOD_SUSY15.23646065._000041.pool.root.1 > files.txt

# make ntuples
xAH_run.py \
--config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/source/DHNLAlgorithm/data/config_DHNLAlgorithm.py \
--inputList \
--files files.txt \
--submitDir submitDir \
--force \
--nevents 1000 \
--extraOptions="--isSUSY15" \
direct
