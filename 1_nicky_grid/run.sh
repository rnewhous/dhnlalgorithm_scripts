xAH_run.py --force \
--config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm_rerunVSIconfigs.py \
--files user.dtrischu.data18_13TeV.periodAllYear.physics_Main.PhysCont.SUSY15.pro25_v04_combLRTd0Test_1p0ONLY_v3_EXT0 \
--inputRucio \
--extraOptions="--isSUSY15 --rerunVSI_LRTR3test" \
prun \
--optGridMergeOutput 20 \
--optGridOutputSampleName user.rnewhous.data18_13TeV.periodAllYear.dHNLnutps.SUSY15.pro25_v04_combLRTd0Test_1p0ONLY_v3_v1 
