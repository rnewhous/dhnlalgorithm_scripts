# mc
# make files.txt
ls -d1 /data/hnl/KShort/SUSY15/dijet_SUSY15/* > dijet_files.txt

# make ntuples
xAH_run.py \
--config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/source/DHNLAlgorithm/data/config_DHNLAlgorithm.py \
--inputList \
--files dijet_files.txt \
--submitDir /data/hnl/KShort/SUSY15/dijet_ntuple/ \
--force \
--isMC \
--extraOptions="--altVSIstr _2  --isSUSY15" \
direct &

# data
# make files.txt
ls -d1 /data/hnl/KShort/SUSY15/data_SUSY15/* > data_files.txt

# make ntuples
xAH_run.py \
--config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/source/DHNLAlgorithm/data/config_DHNLAlgorithm.py \
--inputList \
--files data_files.txt \
--submitDir /data/hnl/KShort/SUSY15/data_ntuple/ \
--force \
--extraOptions="--altVSIstr _2  --isSUSY15" \
direct &