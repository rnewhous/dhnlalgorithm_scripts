# mc
# make files.txt
# 'ls' -1d /data/hnl/KShort/SUSY15/dijet/user.rnewhous.364712.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12WithSW.mc16e.DAOD_SUSY15_v3_EXT0/*.root > files.txt
echo /data/hnl/KShort/SUSY15/dijet/user.rnewhous.364712.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12WithSW.mc16e.DAOD_SUSY15_v4_EXT0/user.rnewhous.22965929.EXT0._000111.DAOD_SUSY15.output.pool.root > files.txt

# make ntuples
xAH_run.py \
--config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/source/DHNLAlgorithm/data/config_DHNLAlgorithm_rerunVSIconfigs.py \
--inputList \
--files files.txt \
--submitDir /tmp/newhouse/submitDir \
--force \
--isMC \
--extraOptions="--isSUSY15 --rerunVSI_LRTR3test" \
--nevents 1000 \
direct

# --log-level critical \
