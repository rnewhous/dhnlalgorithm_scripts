# mc
# make files.txt
"ls" -1d /data/hnl/ttbar_MC/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.DAOD_RPVLL.e6337_e5984_s3126_r11915_r11748/DAOD_RPVLL.* > files.txt

# make ntuples
xAH_run.py \
--config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/source/DHNLAlgorithm/data/config_DHNLAlgorithm.py \
--inputList \
--files files.txt \
--submitDir /data/newhouse/HNL/dijet/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.DAOD_RPVLL.e6337_e5984_s3126_r11915_r11748/submitDir_Leptons/ \
--force \
--isMC \
--extraOptions="--altVSIstr _Leptons" \
direct
