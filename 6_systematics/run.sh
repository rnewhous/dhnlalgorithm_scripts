# mc
# make files.txt
# 'ls' -1d /data/hnl/KShort/SUSY15/dijet/user.rnewhous.364712.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12WithSW.mc16e.DAOD_SUSY15_v3_EXT0/*.root > files.txt

# make ntuples
xAH_run.py \
--config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/source/DHNLAlgorithm/data/config_DHNLAlgorithm.py \
--inputList \
--files files.txt \
--submitDir submitDir/ \
--force \
--isMC \
--nevents 3 \
--extraOptions="--runAllSyst " \
direct
