#!/usr/bin/env bash
#PBS -l walltime=02:00:00
#PBS -l nodes=1:ppn=4
#PBS -q batch
#PBS -N build_DHNL_algo

# setupATLAS
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh

# setup dhnl stup
cd /home/colm/dhnl/DHNLAlgorithm/build
source setup.sh
asetup
source */setup.sh

# run analysis
ls -1 /data/hnl/MC16_DAOD/DAOD_RPVLL/new_MCrequest/promptmuon/mc16_13TeV.311633.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_10G_lt10dd.recon.DAOD_RPVLL.e7422_e5984_a875_r11891/* > data.txt
cd ../run/
xAH_run.py --config ../source/DHNLAlgorithm/data/config_DHNLAlgorithm.py --inputlist --files data.txt --isMC --submitDir testRun --force direct
