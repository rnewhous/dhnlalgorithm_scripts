import glob
import os
import time
import ROOT

DERIV_DIR = '/data/newhouse/HNL/mc/'
RUN_DIR = '/data/newhouse/HNL/ntuples/v9p0/'
WorkDir_DIR='/home/newhouse/public/Analysis/HNL/DHNLAlgorithm/build/generic'
NTUP_VERSION='v9p0'
RUN_ATTEMPT=1


# for sample_list in glob.glob('/home/newhouse/public/Analysis/HNL/DHNLAlgorithm/run/10_susy15_grid/official_lists/DAOD_EXOT29_mc16*'):
for sample_list in glob.glob('/home/newhouse/public/Analysis/HNL/DHNLAlgorithm/run/18_flashy/retry_list.txt'):
    # print(sample_list)
    with open(sample_list) as f:
        for l in f.readlines():
            line = l.strip()

            # in case we need to download
            if not os.path.isdir(f'{DERIV_DIR}{line}'):
                print('\t rucio download', line, '&')
            DISD = line.split('.')[1]
            if 'r11915' in line: mc = 'mc16a'
            if 'r11916' in line: mc = 'mc16d'
            if 'r11891' in line: mc = 'mc16e'
            name = f'{DISD}_{mc}'

            # make running directory
            running_dir = f'{RUN_DIR}{line.replace("deriv.DAOD_EXOT29", "NTUPLE")}.{NTUP_VERSION}/'
            os.makedirs(running_dir, exist_ok=True)

            # make input files list
            # nevents = 0
            with open(f'{running_dir}files.txt', 'w') as f:
                f.write('\n'.join(glob.glob(f'{DERIV_DIR}{line}/*')))
                f.write('\n')

            #     for infile in glob.glob(f'{DERIV_DIR}{line}/*'):
            #         ff = ROOT.TFile(infile)
            #         tt = ff.Get('CollectionTree')
            #         nevents += tt.GetEntries()
            # print(line)
            # print('nevents:', nevents)

            # make submission script
            command = f'qsub -v RUNNING_DIR={running_dir},SAMPLE_PERIOD={mc} -N dhnlalg_{name} -e /home/newhouse/tmp/grid_logs/dhnlalg_{name}.err -o /home/newhouse/tmp/grid_logs/dhnlalg_{name}.log /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/run/18_flashy/submit.pbs'

            max_job_count=100
            def run_count(): return int(os.popen('showq -r|  wc -l').read().strip()) - 4
            def idle_count(): return int(os.popen('showq -i|  wc -l').read().strip()) - 4
            while run_count() + idle_count() > max_job_count:
                time.sleep(10)

            print(command)
            os.system(command)
            print()
            time.sleep(0.1)
            # break