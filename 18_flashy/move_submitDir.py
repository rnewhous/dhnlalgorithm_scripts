import sys
import os
import glob
import uproot

for f in glob.glob('/tmp/newhouse/data/newhouse/HNL/ntuples/v9p0/*/submitDir/data-tree/files.root'):
    file_size = os.path.getsize(f)
    # print("File Size is :", int(file_size/1e6), "MB")
    try:
        nevents = len(uproot.open(f)['nominal']['bcid'].array())
    except:
        continue

    if nevents > 1000:
        os.system(f'mv {f.replace("data-tree/files.root", "")} {f.replace("/tmp/newhouse","").replace("submitDir/data-tree/files.root", "")}')

