# mc
# make files.txt
# 'ls' /data/newhouse/HNL/EXOT29/mc16_13TeV.311633.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_10G_lt10dd.deriv.DAOD_EXOT29.e7422_e5984_a875_r11915_r11748_p4482/* -1d > files_a.txt
# 'ls' /data/newhouse/HNL/EXOT29/mc16_13TeV.311633.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_10G_lt10dd.deriv.DAOD_EXOT29.e7422_e5984_a875_r11891_r11748_p4482/* -1d > files.txt
'ls' /data/newhouse/HNL/EXOT29/mc16_13TeV.312990.Pythia8EvtGen_A14NNPDF23LO_WelHNL50_10G_lt10dd_emu.deriv.DAOD_EXOT29.e7902_e5984_a875_r11891_r11748_p4482/* -1d > files.txt

# make ntuples
xAH_run.py \
--config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
--inputList \
--files files.txt \
--submitDir submitDir_1000 \
--force \
--isMC \
--nevents 1000 \
--extraOptions="--isDerivation --samplePeriod mc16e --runAllSyst" \
direct 

# --nevents 100 \

# # mc
# # make files.txt
# # 'ls' /data/newhouse/HNL/EXOT29/mc16_13TeV.311633.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_10G_lt10dd.deriv.DAOD_EXOT29.e7422_e5984_a875_r11915_r11748_p4482/* -1d > files.txt
# 'ls' /data/newhouse/HNL/EXOT29/mc16_13TeV.311633.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_10G_lt10dd.deriv.DAOD_EXOT29.e7422_e5984_a875_r11891_r11748_p4482/* -1d > files_e.txt
# # 'ls' /data/newhouse/HNL/EXOT29/mc16_13TeV.312990.Pythia8EvtGen_A14NNPDF23LO_WelHNL50_10G_lt10dd_emu.deriv.DAOD_EXOT29.e7902_e5984_a875_r11891_r11748_p4482/* -1d > files.txt

# # make ntuples
# xAH_run.py \
# --config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
# --inputList \
# --files files_e.txt \
# --submitDir submitDir.sfe \
# --force \
# --isMC \
# --nevents 100 \
# --extraOptions="--isDerivation --samplePeriod mc16e --runAllSyst" \
# direct &


# # data
# # make files.txt
# 'ls' /data/newhouse/HNL/EXOT29/data18_13TeV.00358985.physics_Main.deriv.DAOD_EXOT29.r11969_r11784_p4072_p4491/* -1d > files_data.txt

# # make ntuples
# xAH_run.py \
# --config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm.py \
# --inputList \
# --files files_data.txt \
# --submitDir submitDir_data \
# --force \
# --nevents 1000 \
# --extraOptions="--isDerivation --samplePeriod data18" \
# direct &

