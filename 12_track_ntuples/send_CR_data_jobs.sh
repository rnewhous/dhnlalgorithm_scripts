xAH_run.py --force \
--config ../../source/DHNLAlgorithm/data/config_DHNLAlgorithm_alltracks.py \
--files user.dtrischu.data18_13TeV.periodAllYear.physics_Main.PhysCont.SUSY15.pro25_v04_CRstudies_EXT0 \
--inputRucio \
--extraOptions="--isSUSY15" \
prun \
--optGridMergeOutput 20 \
--optGridOutputSampleName user.rnewhous.data18_13TeV.periodAllYear.pro25_v04_CRstudies_savetracks2

#--optGridExcludedSite CA-VICTORIA-WESTGRID-T2_SCRATCHDISK,FZK-LCG2_SCRATCHDISK,IN2P3-CC_SCRATCHDISK,TOKYO-LCG2_SCRATCHDISK,TRIUMF-LCG2_SCRATCHDISK,UKI-NORTHGRID-MAN-HEP_SCRATCHDISK,BNL-OSG2_SCRATCHDISK,NDGF-T1_SCRATCHDISK \
