# mc
# make files.txt
'ls' /data/hnl/VSI_rerun/mc16_13TeV.312958.Pythia8EvtGen_A14NNPDF23LO_WelHNL50_3G_lt100dd_ee_SUSY15_comboLRTd0Test_VSILepMod/* -1d > files.txt
# 'ls' /data/hnl/VSI_rerun/mc16_13TeV.312957.Pythia8EvtGen_A14NNPDF23LO_WelHNL50_3G_lt10dd_ee_SUSY15_comboLRTd0Test_VSILepMod/* -1d > files.txt
# 'ls' /data/hnl/VSI_rerun/mc16_13TeV.312956.Pythia8EvtGen_A14NNPDF23LO_WelHNL50_3G_lt1dd_ee_SUSY15_comboLRTd0Test_VSILepMod/* -1d > files.txt
# 'ls' /data/hnl/VSI_rerun/mc16_13TeV.311602.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_3G_ltdd_SUSY15_comboLRTd0Test_VSILepMod/* -1d > files.txt
# 'ls' /data/hnl/VSI_rerun/mc16_13TeV.311604.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_3G_lt100dd_SUSY15_comboLRTd0Test_VSILepMod/* -1d > files.txt
# 'ls' /data/hnl/VSI_rerun/mc16_13TeV.312959.Pythia8EvtGen_A14NNPDF23LO_WelHNL50_3G_lt1dd_emu_SUSY15_comboLRTd0Test_VSILepMod/* -1d > files.txt
# 'ls' /data/hnl/VSI_rerun/mc16_13TeV.311603.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_3G_lt10dd_SUSY15_comboLRTd0Test_VSILepMod/* -1d > files.txt
# 'ls' /data/hnl/VSI_rerun/mc16_13TeV.311605.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_3G_lt1dd_el_SUSY15_comboLRTd0Test_VSILepMod/* -1d > files.txt
# 'ls' /data/hnl/VSI_rerun/mc16_13TeV.311607.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_3G_lt100dd_el_SUSY15_comboLRTd0Test_VSILepMod/* -1d > files.txt
# 'ls' /data/hnl/VSI_rerun/mc16_13TeV.312961.Pythia8EvtGen_A14NNPDF23LO_WelHNL50_3G_lt100dd_emu_SUSY15_comboLRTd0Test_VSILepMod/* -1d > files.txt
# 'ls' /data/hnl/VSI_rerun/mc16_13TeV.311651.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_17p5G_lt10dd_SUSY15_comboLRTd0Test_VSILepMod/* -1d > files.txt
# 'ls' /data/hnl/VSI_rerun/mc16_13TeV.311639.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_12p5G_lt10dd_SUSY15_comboLRTd0Test_VSILepMod/* -1d > files.txt

# 'ls' /data/hnl/VSI_rerun/mc16_13TeV.311607.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_3G_lt100dd_el_SUSY15_comboLRTd0Test_VSILepMod/* -1d > files.txt

# make ntuples
xAH_run.py \
--config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/source/DHNLAlgorithm/data/config_DHNLAlgorithm.py \
--inputList \
--files files.txt \
--submitDir 312958 \
--force \
--isMC \
--extraOptions="--isSUSY15" \
direct

# --nevents 1000 \
