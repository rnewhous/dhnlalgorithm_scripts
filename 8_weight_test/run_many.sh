# mc
# make files.txt
for f in \
/data/hnl/MC16_DAOD/DAOD_RPVLL/new_MCrequest/promptmuon/mc16_13TeV.311636.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_10G_lt10dd_el.merge.DAOD_RPVLL.e7422_e5984_a875_r11891_r11748 \
# /data/hnl/VSI_rerun/mc16_13TeV.311604.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_3G_lt100dd_SUSY15_comboLRTd0Test_VSILepMod \
# /data/hnl/VSI_rerun/mc16_13TeV.312959.Pythia8EvtGen_A14NNPDF23LO_WelHNL50_3G_lt1dd_emu_SUSY15_comboLRTd0Test_VSILepMod \
# /data/hnl/VSI_rerun/mc16_13TeV.311603.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_3G_lt10dd_SUSY15_comboLRTd0Test_VSILepMod \
# /data/hnl/VSI_rerun/mc16_13TeV.311605.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_3G_lt1dd_el_SUSY15_comboLRTd0Test_VSILepMod \
# /data/hnl/VSI_rerun/mc16_13TeV.311607.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_3G_lt100dd_el_SUSY15_comboLRTd0Test_VSILepMod \
# /data/hnl/VSI_rerun/mc16_13TeV.312961.Pythia8EvtGen_A14NNPDF23LO_WelHNL50_3G_lt100dd_emu_SUSY15_comboLRTd0Test_VSILepMod \
# /data/hnl/VSI_rerun/mc16_13TeV.311651.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_17p5G_lt10dd_SUSY15_comboLRTd0Test_VSILepMod \
# /data/hnl/VSI_rerun/mc16_13TeV.311639.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_12p5G_lt10dd_SUSY15_comboLRTd0Test_VSILepMod 
do
    mkdir -p ~/tmp/weight_test/${f:31:6}_mc16e
    cd  ~/tmp/weight_test/${f:31:6}_mc16e
    'ls' $f/* -1d > files.txt
    # make ntuples
    xAH_run.py \
    --config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/source/DHNLAlgorithm/data/config_DHNLAlgorithm.py \
    --inputList \
    --files files.txt \
    --submitDir submitDir \
    --force \
    --isMC \
    --extraOptions="--isSUSY15" \
    direct &
done



# /data/hnl/VSI_rerun/mc16_13TeV.312958.Pythia8EvtGen_A14NNPDF23LO_WelHNL50_3G_lt100dd_ee_SUSY15_comboLRTd0Test_VSILepMod \
# /data/hnl/VSI_rerun/mc16_13TeV.312957.Pythia8EvtGen_A14NNPDF23LO_WelHNL50_3G_lt10dd_ee_SUSY15_comboLRTd0Test_VSILepMod \
# /data/hnl/VSI_rerun/mc16_13TeV.312956.Pythia8EvtGen_A14NNPDF23LO_WelHNL50_3G_lt1dd_ee_SUSY15_comboLRTd0Test_VSILepMod \
# /data/hnl/VSI_rerun/mc16_13TeV.311602.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_3G_ltdd_SUSY15_comboLRTd0Test_VSILepMod \

xAH_run.py --config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/source/DHNLAlgorithm/data/config_DHNLAlgorithm.py --inputList --files files_mc16a.txt --submitDir submitDir_mc16a_fixed --force --isMC direct & 
xAH_run.py --config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/source/DHNLAlgorithm/data/config_DHNLAlgorithm.py --inputList --files files_mc16d.txt --submitDir submitDir_mc16d_fixed --force --isMC direct & 
xAH_run.py --config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/source/DHNLAlgorithm/data/config_DHNLAlgorithm.py --inputList --files files_mc16e.txt --submitDir submitDir_mc16e_fixed --force --isMC direct & 