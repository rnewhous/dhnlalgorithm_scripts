# mc
# make files.txt
ls -d1 /data/newhouse/test/DAOD_SUSY15.ttbar1_VSILeptons2_output.pool.root > files.txt

# make ntuples
xAH_run.py \
--config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/source/DHNLAlgorithm/data/config_DHNLAlgorithm.py \
--inputList \
--files files.txt \
--submitDir /data/newhouse/test/submitDir \
--force \
--isMC \
--extraOptions="--altVSIstr _Leptons2  --isSUSY15 --noPRW" \
direct