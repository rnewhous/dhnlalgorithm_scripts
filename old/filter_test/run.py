from __future__ import print_function
import os
import argparse
import subprocess

parser = argparse.ArgumentParser(description='Pick directory for testing')
parser.add_argument('--input_dir', dest='input_dir', required=True)
args = parser.parse_args()

# clean dir
args.input_dir = os.path.join(args.input_dir, '')

is_RPVLL = 'DAOD_RPVLL' in args.input_dir

print("input_dir", args.input_dir)

# output_dir = "testRun_DAOD_RPVLL" if is_RPVLL else "testRun_AOD"
parent_dir = os.path.join(os.path.dirname(args.input_dir), '..')
output_dir = os.path.abspath(os.path.join(parent_dir, 'ntuple_DAOD_RPVLL' if is_RPVLL else 'ntuple_AOD'))
config_file = "config_DHNLAlgorithm.py" if is_RPVLL else "config_filter_only.py"
file_list = os.path.join(parent_dir, "DAOD_RPVLL_files.txt" if is_RPVLL else "AOD_files.txt")

with open(file_list, 'w') as ff:
    for dirpath,_,filenames in os.walk(args.input_dir):
        for f in filenames:
            ff.write(os.path.abspath(os.path.join(dirpath, f))+'\n')

command = '''
xAH_run.py \
--config $WorkDir_DIR/data/DHNLAlgorithm/{} \
--inputList \
--files {} \
--submitDir {} \
--force \
--isMC \
direct &
'''.format(config_file, file_list, output_dir)

os.system(command)
