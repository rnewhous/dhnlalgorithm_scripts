# mc
# make files.txt
ls -d1 /data/newhouse/test/mc16_13TeV.361050.Sherpa_CT10_SinglePhotonPt280_500_BFilter.recon.DAOD_RPVLL.e3587_e5984_s3126_r11916/* > files.txt
# ls -d1 /data/hnl/MC16_DAOD/DAOD_RPVLL/new_MCrequest/promptmuon/mc16_13TeV.311657.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_20G_lt10dd.recon.DAOD_RPVLL.e7422_e5984_a875_r11891/* > files.txt

# make ntuples
xAH_run.py \
--config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/source/DHNLAlgorithm/data/config_DHNLAlgorithm.py \
--inputList \
--files files.txt \
--submitDir submitDir \
--force \
--isMC \
--log-level debug \
--extraOptions="--noPRW" \
direct
