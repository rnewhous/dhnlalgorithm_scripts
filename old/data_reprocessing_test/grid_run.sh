# These are the test samples from reprocessing request https://its.cern.ch/jira/browse/DATREP-183

# SAMPLE=data18_13TeV.00358031.physics_Main.merge.DAOD_RPVLL.r11760_r11764_p4054 # Unaligned
SAMPLE=data18_13TeV.00358031.physics_Main.merge.DAOD_RPVLL.r11969_r11784_p4072_tid21711692_00 # Aligned
OUTPUT_NAME=data18_13TeV.00358031.physics_Main.merge.DAOD_RPVLL.r11969_r11784_p4072
xAH_run.py --config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm.py --files $SAMPLE --inputRucio --submitDir submitDir_data18 --force prun --optGridMergeOutput 1 --optGridOutputSampleName user.${CERN_USER}.${OUTPUT_NAME}_HNLNtuple_01 --optGridNGBPerJob 4

# SAMPLE=data17_13TeV.00340368.physics_Main.merge.DAOD_RPVLL.r11761_r11764_p4054 # Unaligned
SAMPLE=data17_13TeV.00340368.physics_Main.merge.DAOD_RPVLL.r11969_r11784_p4072_tid21711746_00 # Aligned
OUTPUT_NAME=data17_13TeV.00340368.physics_Main.merge.DAOD_RPVLL.r11969_r11784_p4072 
xAH_run.py --config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm.py --files $SAMPLE --inputRucio --submitDir submitDir_data17 --force prun --optGridMergeOutput 1 --optGridOutputSampleName user.${CERN_USER}.${OUTPUT_NAME}_HNLNtuple_01 --optGridNGBPerJob 4

# SAMPLE=data16_13TeV.00304178.physics_Main.merge.DAOD_RPVLL.r11761_r11764_p4054 # Unaligned
SAMPLE=data16_13TeV.00304178.physics_Main.merge.DAOD_RPVLL.r11969_r11784_p4072_tid21711783_00 # Aligned
OUTPUT_NAME=data16_13TeV.00304178.physics_Main.recon.DAOD_RPVLL.r11969_r11784_p4072 
xAH_run.py --config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm.py --files $SAMPLE --inputRucio --submitDir submitDir_data16 --force prun --optGridMergeOutput 1 --optGridOutputSampleName user.${CERN_USER}.${OUTPUT_NAME}_HNLNtuple_01 --optGridNGBPerJob 4

# SAMPLE=data15_13TeV.00284427.physics_Main.merge.DAOD_RPVLL.r11761_r11764_p4054 # Unaligned
SAMPLE=data15_13TeV.00284427.physics_Main.merge.DAOD_RPVLL.r11969_r11784_p4072_tid21712147_00 # Aligned
OUTPUT_NAME=data15_13TeV.00284427.physics_Main.merge.DAOD_RPVLL.r11969_r11784_p4072
xAH_run.py --config $WorkDir_DIR/data/DHNLAlgorithm/config_DHNLAlgorithm.py --files $SAMPLE --inputRucio --submitDir submitDir_data15 --force prun --optGridMergeOutput 1 --optGridOutputSampleName user.${CERN_USER}.${OUTPUT_NAME}_HNLNtuple_01 --optGridNGBPerJob 4
