# make ntuples
'ls' -1d /data/newhouse/HNL/EXOT29/mc16_13TeV.311633.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_10G_lt10dd.deriv.DAOD_EXOT29.e7422_e5984_a875_r11891_r11748_p4482/* > uuu_files.txt
xAH_run.py \
--config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/source/DHNLAlgorithm/data/config_DHNLAlgorithm.py \
--inputList \
--files uuu_files.txt \
--submitDir uuu \
--force \
--isMC \
--extraOptions="--isDerivation" \
direct &


# make ntuples
'ls' -1d /data/newhouse/HNL/EXOT29/mc16_13TeV.311636.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_10G_lt10dd_el.deriv.DAOD_EXOT29.e7422_e5984_a875_r11891_r11748_p4482/* > uue_files.txt
xAH_run.py \
--config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/source/DHNLAlgorithm/data/config_DHNLAlgorithm.py \
--inputList \
--files uue_files.txt \
--submitDir uue \
--force \
--isMC \
--extraOptions="--isDerivation" \
direct &


# make ntuples
'ls' -1d /data/newhouse/HNL/EXOT29/mc16_13TeV.312990.Pythia8EvtGen_A14NNPDF23LO_WelHNL50_10G_lt10dd_emu.deriv.DAOD_EXOT29.e7902_e5984_a875_r11891_r11748_p4482/* > eeu_files.txt
xAH_run.py \
--config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/source/DHNLAlgorithm/data/config_DHNLAlgorithm.py \
--inputList \
--files eeu_files.txt \
--submitDir eeu \
--force \
--isMC \
--extraOptions="--isDerivation" \
direct &

# make ntuples
'ls' -1d /data/newhouse/HNL/EXOT29/mc16_13TeV.312987.Pythia8EvtGen_A14NNPDF23LO_WelHNL50_10G_lt10dd_ee.deriv.DAOD_EXOT29.e7902_e5984_a875_r11891_r11748_p4482/* > eee_files.txt
xAH_run.py \
--config /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/source/DHNLAlgorithm/data/config_DHNLAlgorithm.py \
--inputList \
--files eee_files.txt \
--submitDir eee \
--force \
--isMC \
--extraOptions="--isDerivation" \
direct &
